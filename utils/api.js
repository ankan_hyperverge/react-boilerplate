/* eslint-disable import/prefer-default-export */
export const fetchTodoById = async (id) => {
  const response = await fetch(
    `https://jsonplaceholder.typicode.com/todos/${id}`,
  );
  if (response.status !== 200) {
    return Promise.reject(new Error('Error while fetching todo'));
  }
  return response.json();
};
