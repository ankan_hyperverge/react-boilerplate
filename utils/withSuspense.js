import React, { Suspense } from 'react';

const getDisplayName = (Component) =>
  Component.displayName || Component.name || 'Component';

const withSuspense = (WrappedComponent) => {
  const WithSuspense = (props) => {
    const fallback = null;

    return (
      <Suspense fallback={fallback}>
        <WrappedComponent {...props} />
      </Suspense>
    );
  };

  WithSuspense.displayName = `WithSuspense(${getDisplayName(
    WrappedComponent,
  )})`;

  return WithSuspense;
};

export default withSuspense;
