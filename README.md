# React Boilerplate

<br />

<div align="center"><strong>Start your next react project in seconds</strong></div>
<div align="center">A simple boilerplate for creating a full-fledged react application</div>

<br />

## Getting started

- Make sure that you have Node.js v16.13.2 and npm v8 or above installed.
- Clone this repo using git clone --depth=1 https://github.com/react-boilerplate/react-boilerplate.git <YOUR_PROJECT_NAME>
- Move to the appropriate directory: cd <YOUR_PROJECT_NAME>.
- Clean up the `app/store.js`, `app/containers/App/index.jsx` and accordingly, other files inside the `app/containers` directory.
- You can run `npm start` to see the example app at http://localhost:1234.

## Tools

### Core

- [React](https://reactjs.org/) - React v17 has been used in this boilerplate
- [React Router](https://reactrouter.com/) - React-router-dom v6 has been used for routing
- [Redux Toolkit](https://redux-toolkit.js.org/) - Redux Toolkit is the official, opinionated, batteries-included toolset for efficient Redux development which makes redux development way easier than the traditional way
- [Parcel](https://parceljs.org/) - Parcel is a zero-configuration build tool

### Unit Testing

- [Jest](https://jestjs.io/) - Jest is the Test runner framework
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/) - React Testing Library provides testing utilities for writing tests for React apps

### Linting

- [ESLint](http://eslint.org/) - Favourite linter for JavaScript and TypeScript
- [Prettier](https://prettier.io/) - Code-formatter so that you don't commit dirty code!
- [stylelint](https://stylelint.io/) - Linter for your style files

## Roadmap
- Adding `setup` and `clean` scripts to automate cleaning up of current repo for ease of use.

## Author
Ankan Poddar ([ankan@hyperverge.co](mailto:ankan@hyperverge.co))

## License
This project is licensed under the MIT license, Copyright (c) 2022 Ankan Poddar. For more information see `LICENSE.md`.
