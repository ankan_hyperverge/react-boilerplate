module.exports = {
  collectCoverageFrom: [
    'app/**/*.{js,jsx}',
    '!app/**/*.test.{js,jsx}',
    '!app/*/RbGenerated*/*.{js,jsx}',
    '!app/index.js',
    '!app/*/*/Loadable.{js,jsx}',
  ],
  coverageThreshold: {
    global: {
      statements: 98,
      branches: 91,
      functions: 98,
      lines: 98,
    },
  },
  moduleDirectories: ['node_modules', 'app'],
  moduleNameMapper: {
    '.*\\.(css|less|styl|scss|sass)$': '<rootDir>/internals/mocks/cssModule.js',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/internals/mocks/image.js',
  },
  testPathIgnorePatterns: ['<rootDir>/node_modules/*'],
  testRegex: 'tests/.*\\.test\\.js$',
  snapshotSerializers: [],
  testEnvironment: 'jest-environment-jsdom',
  transform: {
    '\\.js$': '<rootDir>/node_modules/babel-jest',
    '\\.jsx$': '<rootDir>/node_modules/babel-jest',
  },
};
