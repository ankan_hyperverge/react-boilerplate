import { configureStore } from '@reduxjs/toolkit';

import counterReducer from './containers/Counter/slice';
import todoReducer from './containers/TodoList/slice';

const store = configureStore({
  reducer: { counter: counterReducer, todo: todoReducer },
});

export default store;
