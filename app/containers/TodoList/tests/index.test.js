import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { render, fireEvent, within, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import TodoList from '../index';
import store from '../../../store';
import * as apiClient from '../../../../utils/api';

let component;

const errorSpy = jest.spyOn(global.console, 'error');
const warningSpy = jest.spyOn(global.console, 'warn');

const mockNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockNavigate,
}));

/* It is recommended to test components that include Redux code via integration
tests that include everything working together, with assertions aimed at
verifying that the app behaves the way it is expected to when the user
interacts with it in a given manner */

describe('<TodoList />', () => {
  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <BrowserRouter>
          <TodoList />
        </BrowserRouter>
      </Provider>,
    );
  });

  afterEach(() => jest.clearAllMocks());

  it('Does not log errors in console', () => {
    expect(errorSpy).not.toHaveBeenCalled();
  });

  it('Does not log warnings in console', () => {
    expect(warningSpy).not.toHaveBeenCalled();
  });

  it('Displays inputs and buttons', () => {
    const addTodoInput = component.getByTestId('add-todo-input');
    const addTodoBtn = component.getByTestId('add-todo-btn');
    const fetchTodoBtn = component.getByTestId('fetch-todo-btn');
    expect(addTodoInput).toBeVisible();
    expect(addTodoBtn).toBeVisible();
    expect(fetchTodoBtn).toBeVisible();
  });

  it('Adds new todo from input box', async () => {
    const addTodoInput = component.getByTestId('add-todo-input');
    const addTodoBtn = component.getByTestId('add-todo-btn');
    const newTodo = 'new todo';
    fireEvent.change(addTodoInput, { target: { value: newTodo } });
    fireEvent.click(addTodoBtn);
    expect(component.queryByText(newTodo)).not.toBeNull();
    expect(addTodoInput).toHaveDisplayValue('');
  });

  it('If todo input is empty, new todo is not added', async () => {
    const addTodoBtn = component.getByTestId('add-todo-btn');
    const todoList = component.getByTestId('todo-list');
    const prevTodoListLength = todoList.children.length;
    fireEvent.click(addTodoBtn);
    expect(todoList.children.length).toEqual(prevTodoListLength);
  });

  it('Delete todo by clicking on delete button', async () => {
    const todoList = component.getByTestId('todo-list');
    const todoText = within(todoList.children[0]).getByTestId(
      'todo-title',
    ).textContent;
    const deleteButton = within(todoList.children[0]).getByTestId(
      'todo-delete-btn',
    );
    fireEvent.click(deleteButton);
    expect(within(todoList).queryByText(todoText)).toBeNull();
  });

  it('Toggle todo by clicking on checkbox', async () => {
    const todoList = component.getByTestId('todo-list');
    const todoCheckbox = within(todoList.children[0]).getByTestId(
      'todo-checkbox',
    );
    const todoCheckStatus = todoCheckbox.checked;
    fireEvent.click(todoCheckbox);
    expect(todoCheckbox.checked).toBe(!todoCheckStatus);
  });

  it('Fetches and adds todo by clicking on fetch button', async () => {
    const newTodo = {
      title: 'xyz',
      completed: false,
    };
    jest.spyOn(apiClient, 'fetchTodoById').mockResolvedValueOnce(newTodo);
    const fetchTodoBtn = component.getByTestId('fetch-todo-btn');
    fireEvent.click(fetchTodoBtn);
    expect(component.getByTestId('fetch-todo-btn')).toHaveTextContent(
      /loading/i,
    );
    await waitFor(() =>
      expect(component.queryByText(newTodo.title)).not.toBeNull(),
    );
  });

  it('Calls navigate with -1 when Back button is clicked', () => {
    const backBtn = component.getByTestId('back-btn');
    fireEvent.click(backBtn);
    expect(mockNavigate).toBeCalledWith(-1);
  });
});
