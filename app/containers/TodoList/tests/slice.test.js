import reducer, {
  initialState,
  addTodo,
  deleteTodo,
  toggleTodo,
} from '../slice';

describe('TodoSlice', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  const prevState = {
    todos: [
      { id: 0, title: 'xyz', isCompleted: true },
      { id: 1, title: 'abc', isCompleted: false },
    ],
  };

  it('Should add todo', () => {
    const newTodo = { title: 'teedee', isCompleted: true };
    const newState = reducer(
      prevState,
      addTodo(newTodo.title, newTodo.isCompleted),
    );
    expect(newState.todos).toHaveLength(prevState.todos.length + 1);
    expect(newState.todos[prevState.todos.length]).toEqual({
      id: expect.anything(),
      title: newTodo.title,
      isCompleted: newTodo.isCompleted,
    });
  });

  it('Should delete todo', () => {
    const todoToDelete = 1;
    const newState = reducer(prevState, deleteTodo(todoToDelete));
    expect(newState.todos).toHaveLength(prevState.todos.length - 1);
    expect(
      newState.todos.find((todo) => todo.id === todoToDelete),
    ).not.toBeDefined();
  });

  it('Does nothing if todo with id is not found in deleteTodo', () => {
    const todoToDelete = 2;
    const newState = reducer(prevState, deleteTodo(todoToDelete));
    expect(newState).toEqual(prevState);
  });

  it('Should toggle todo', () => {
    const todoToToggle = 1;
    const newState = reducer(prevState, toggleTodo(todoToToggle));
    expect(newState.todos).toHaveLength(prevState.todos.length);
    expect(newState.todos[todoToToggle].isCompleted).toEqual(
      !prevState.todos[todoToToggle].isCompleted,
    );
  });

  it('Does nothing if todo with id is not found in toggleTodo', () => {
    const todoToToggle = 2;
    const newState = reducer(prevState, toggleTodo(todoToToggle));
    expect(newState).toEqual(prevState);
  });
});
