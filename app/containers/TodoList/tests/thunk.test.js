import * as apiClient from '../../../../utils/api';
import store from '../../../store';
import { initialState } from '../slice';
import { fetchTodo } from '../thunk';

describe('TodoThunk', () => {
  it('Should add todo to state if promise is fulfilled', async () => {
    const newTodo = {
      title: 'xyz',
      completed: false,
    };
    const fetchTodoByIdSpy = jest
      .spyOn(apiClient, 'fetchTodoById')
      .mockResolvedValueOnce(newTodo);
    await store.dispatch(fetchTodo());
    expect(fetchTodoByIdSpy).toBeCalledWith(initialState.todoCounter);
    const newState = store.getState();
    expect(newState.todo.todos).toEqual([
      ...initialState.todos,
      {
        id: expect.anything(),
        title: newTodo.title,
        isCompleted: newTodo.completed,
      },
    ]);
  });

  it('Should set loading to true while promise is pending', () => {
    const newTodo = {
      title: 'xyz',
      completed: false,
    };
    jest.spyOn(apiClient, 'fetchTodoById').mockResolvedValueOnce(newTodo);
    store.dispatch(fetchTodo());
    const newState = store.getState();
    expect(newState.todo.loading).toBe(true);
  });

  it('Should alert error message if promise is rejected', async () => {
    const error = new Error('Test error message');
    jest.spyOn(apiClient, 'fetchTodoById').mockRejectedValueOnce(error);
    const alertSpy = jest.spyOn(window, 'alert').mockImplementation(() => {});
    await store.dispatch(fetchTodo());
    expect(alertSpy).toBeCalledWith(error.message);
  });
});
