import PropTypes from 'prop-types';

const Todo = (props) => {
  const { title, isCompleted, onCheckChange, onDeleteTodo } = props;
  return (
    <div>
      <input
        data-testid="todo-checkbox"
        type="checkbox"
        checked={isCompleted}
        onChange={onCheckChange}
      />
      <span data-testid="todo-title">{title}</span>
      <button
        data-testid="todo-delete-btn"
        type="button"
        onClick={onDeleteTodo}
      >
        Delete
      </button>
    </div>
  );
};

Todo.propTypes = {
  title: PropTypes.string.isRequired,
  isCompleted: PropTypes.bool.isRequired,
  onCheckChange: PropTypes.func.isRequired,
  onDeleteTodo: PropTypes.func.isRequired,
};

export default Todo;
