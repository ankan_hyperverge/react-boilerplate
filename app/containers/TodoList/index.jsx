import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import Todo from './Todo';
import { addTodo, toggleTodo, deleteTodo } from './slice';
import { fetchTodo } from './thunk';
import withSuspense from '../../../utils/withSuspense';

const TodoList = () => {
  const [newTodoTitle, setNewTodoTitle] = useState('');
  const todos = useSelector((state) => state.todo.todos);
  const fetchTodoLoading = useSelector((state) => state.todo.loading);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const addNewTodo = () => {
    if (!newTodoTitle) return;
    dispatch(addTodo(newTodoTitle, false));
    setNewTodoTitle('');
  };

  return (
    <div>
      <div>
        <button
          data-testid="back-btn"
          type="button"
          onClick={() => navigate(-1)}
        >
          Go Back
        </button>
      </div>
      <div data-testid="todo-list">
        {todos.map((todoDetails) => (
          <Todo
            key={todoDetails.id}
            {...todoDetails}
            onCheckChange={() => dispatch(toggleTodo(todoDetails.id))}
            onDeleteTodo={() => dispatch(deleteTodo(todoDetails.id))}
          />
        ))}
      </div>
      <div>
        <input
          type="text"
          data-testid="add-todo-input"
          placeholder="Add new task"
          value={newTodoTitle}
          onChange={(e) => setNewTodoTitle(e.target.value)}
        />
        <button data-testid="add-todo-btn" type="button" onClick={addNewTodo}>
          Add Todo
        </button>
        <button
          data-testid="fetch-todo-btn"
          type="button"
          disabled={fetchTodoLoading}
          onClick={() => dispatch(fetchTodo())}
        >
          {fetchTodoLoading ? 'Loading...' : 'Fetch Random Todo'}
        </button>
      </div>
    </div>
  );
};

export default withSuspense(TodoList);
