/* eslint-disable no-param-reassign */
import { createSlice, nanoid } from '@reduxjs/toolkit';

import { fetchTodo } from './thunk';

export const initialState = {
  todos: [
    {
      id: nanoid(),
      title: 'fetch vegetables',
      isCompleted: true,
    },
    {
      id: nanoid(),
      title: 'boil eggs',
      isCompleted: false,
    },
  ],
  todoCounter: 2,
  loading: false,
};

const addNewTodo = (state, action) => {
  state.todos.push(action.payload);
  state.todoCounter += 1;
};

const todoSlice = createSlice({
  name: 'todo',
  initialState,
  reducers: {
    addTodo: {
      reducer: addNewTodo,
      prepare: (title, isCompleted) => ({
        payload: {
          id: nanoid(),
          title,
          isCompleted,
        },
      }),
    },
    deleteTodo: (state, action) => {
      const idxToDelete = state.todos.findIndex(
        (todo) => todo.id === action.payload,
      );
      if (idxToDelete !== -1) {
        state.todos.splice(idxToDelete, 1);
      }
    },
    toggleTodo: (state, action) => {
      const todoToToggle = state.todos.find(
        (todo) => todo.id === action.payload,
      );
      if (todoToToggle) {
        todoToToggle.isCompleted = !todoToToggle.isCompleted;
      }
    },
  },
  extraReducers: {
    [fetchTodo.pending]: (state) => {
      state.loading = true;
    },
    [fetchTodo.fulfilled]: (state, action) => {
      state.loading = false;
      addNewTodo(state, {
        payload: {
          id: nanoid(),
          ...action.payload,
        },
      });
    },
    [fetchTodo.rejected]: (state, action) => {
      state.loading = false;
      alert(action.error.message);
    },
  },
});

export const { addTodo, deleteTodo, toggleTodo } = todoSlice.actions;

export default todoSlice.reducer;
