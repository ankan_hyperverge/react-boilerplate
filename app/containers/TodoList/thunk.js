/* eslint-disable import/prefer-default-export */
import { createAsyncThunk } from '@reduxjs/toolkit';

import * as apiClient from '../../../utils/api';

export const fetchTodo = createAsyncThunk(
  'todo/fetchTodo',
  async (_params, thunkAPI) => {
    const { getState } = thunkAPI;
    const {
      todo: { todoCounter },
    } = getState();
    const response = await apiClient.fetchTodoById(todoCounter);
    const { title, completed } = response;
    return { title, isCompleted: completed };
  },
);
