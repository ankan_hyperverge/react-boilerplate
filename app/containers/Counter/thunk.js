/* eslint-disable import/prefer-default-export */
import { createAsyncThunk } from '@reduxjs/toolkit';

export const incrementAsync = createAsyncThunk(
  'counter/incrementAsync',
  async (incrementAmount) => {
    await new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
    return incrementAmount;
  },
);
