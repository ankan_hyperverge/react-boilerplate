import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { decrement, increment, incrementByAmount } from './slice';
import { incrementAsync } from './thunk';
import withSuspense from '../../../utils/withSuspense';

import './Counter.scss';

const Counter = () => {
  const count = useSelector((state) => state.counter);
  const [incrementAmount, setIncrementAmount] = useState('2');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  return (
    <div className="counter">
      <div className="counter__row">
        <button
          className="counter__btn"
          data-testid="increment-btn"
          type="button"
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          +
        </button>
        <span data-testid="count" className="counter__value">
          {count}
        </span>
        <button
          className="counter__btn"
          data-testid="decrement-btn"
          type="button"
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          -
        </button>
      </div>
      <div className="counter__row">
        <input
          className="counter__textbox"
          aria-label="Set increment amount"
          value={incrementAmount}
          onChange={(e) => setIncrementAmount(e.target.value)}
        />
        <button
          className="counter__btn"
          data-test-id="increment-amount-btn"
          type="button"
          aria-label="Increment by amount"
          onClick={() =>
            dispatch(incrementByAmount(Number(incrementAmount) || 0))
          }
        >
          Add Amount
        </button>
        <button
          className="counter__btn_async"
          data-test-id="increment-async-btn"
          type="button"
          aria-label="Increment async by amount"
          onClick={() => dispatch(incrementAsync(Number(incrementAmount) || 0))}
        >
          Add Async
        </button>
      </div>
      <div className="counter__row">
        <button
          className="counter__btn"
          data-testid="back-btn"
          type="button"
          aria-label="Go to previous page"
          onClick={() => navigate(-1)}
        >
          Go Back
        </button>
      </div>
    </div>
  );
};

export default withSuspense(Counter);
