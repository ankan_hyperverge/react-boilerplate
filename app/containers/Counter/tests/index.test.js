import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Counter from '../index';
import store from '../../../store';

let component;

const errorSpy = jest.spyOn(global.console, 'error');
const warningSpy = jest.spyOn(global.console, 'warn');

const mockNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockNavigate,
}));

/* It is recommended to test components that include Redux code via integration
tests that include everything working together, with assertions aimed at
verifying that the app behaves the way it is expected to when the user
interacts with it in a given manner */

describe('<Counter />', () => {
  beforeEach(() => {
    component = render(
      <Provider store={store}>
        <BrowserRouter>
          <Counter />
        </BrowserRouter>
      </Provider>,
    );
  });

  afterEach(() => jest.clearAllMocks());

  it('Does not log errors in console', () => {
    expect(errorSpy).not.toHaveBeenCalled();
  });

  it('Does not log warnings in console', () => {
    expect(warningSpy).not.toHaveBeenCalled();
  });

  it('Displays buttons and count', () => {
    const incrementBtn = component.getByTestId('increment-btn');
    const decrementBtn = component.getByTestId('decrement-btn');
    const countElem = component.getByTestId('count');
    expect(incrementBtn).toBeVisible();
    expect(decrementBtn).toBeVisible();
    expect(countElem).toBeVisible();
  });

  it('Increment button increments the count', () => {
    const incrementBtn = component.getByTestId('increment-btn');
    const countElem = component.getByTestId('count');
    const prevCount = parseInt(countElem.textContent, 10);
    fireEvent.click(incrementBtn);
    expect(countElem).toHaveTextContent(prevCount + 1);
  });

  it('Decrement button increments the count', () => {
    const decrementBtn = component.getByTestId('decrement-btn');
    const countElem = component.getByTestId('count');
    const prevCount = parseInt(countElem.textContent, 10);
    fireEvent.click(decrementBtn);
    expect(countElem).toHaveTextContent(prevCount - 1);
  });

  it('Calls navigate with -1 when Back button is clicked', () => {
    const backBtn = component.getByTestId('back-btn');
    fireEvent.click(backBtn);
    expect(mockNavigate).toBeCalledWith(-1);
  });
});
