import reducer, {
  initialState,
  increment,
  decrement,
  incrementByAmount,
  decrementByAmount,
} from '../slice';

describe('CounterSlice', () => {
  it('Should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('Should handle increment action', () => {
    const prevState = 5;
    expect(reducer(prevState, increment())).toEqual(prevState + 1);
  });

  it('Should handle decrement action', () => {
    const prevState = 5;
    expect(reducer(prevState, decrement())).toEqual(prevState - 1);
  });

  it('Should handle incrementByAmount action', () => {
    const prevState = 5;
    const payload = 2;
    expect(reducer(prevState, incrementByAmount(payload))).toEqual(
      prevState + payload,
    );
  });

  it('Should handle decrementByAmount action', () => {
    const prevState = 5;
    const payload = 2;
    expect(reducer(prevState, decrementByAmount(payload))).toEqual(
      prevState - payload,
    );
  });
});
