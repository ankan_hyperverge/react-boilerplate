import { createSlice } from '@reduxjs/toolkit';

import { incrementAsync } from './thunk';

export const initialState = 0;

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => state + 1,
    decrement: (state) => state - 1,
    incrementByAmount: (state, { payload }) => state + payload,
    decrementByAmount: (state, { payload }) => state - payload,
  },
  extraReducers: {
    [incrementAsync.fulfilled]: (state, action) => state + action.payload,
  },
});

// Action creators are generated for each case reducer function
export const { increment, decrement, incrementByAmount, decrementByAmount } =
  counterSlice.actions;

export default counterSlice.reducer;
