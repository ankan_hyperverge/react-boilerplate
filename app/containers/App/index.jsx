import { Routes, Route, Link } from 'react-router-dom';

import Logo from '../../images/logo.svg';
import Counter from '../Counter';
import TodoList from '../TodoList';

import './App.scss';

const HomePage = () => (
  <div className="home">
    <h1>Sample App made with React, Redux and Parcel!</h1>
    <div className="home__nav">
      <Link to="/counter" className="home__nav-link">
        Counter App
      </Link>
      <br />
      <Link to="/todo" className="home__nav-link">
        Todo App
      </Link>
    </div>
  </div>
);

const NotFoundPage = () => (
  <div style={{ padding: '1rem' }}>
    {/* eslint-disable-next-line react/jsx-curly-brace-presence */}
    <h1>{"There's nothing here!!"}</h1>
  </div>
);

const App = () => (
  <div className="app">
    <img src={Logo} className="app__logo" alt="Logo" />
    <Routes>
      <Route exact path="/" element={<HomePage />} />
      <Route path="todo" element={<TodoList />} />
      <Route path="counter" element={<Counter />} />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  </div>
);

export default App;
