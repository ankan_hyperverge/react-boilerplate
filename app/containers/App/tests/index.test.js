import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import App from '../index';
import store from '../../../store';

const errorSpy = jest.spyOn(global.console, 'error');
const warningSpy = jest.spyOn(global.console, 'warn');

describe('<App />', () => {
  beforeEach(() => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>,
    );
  });

  afterEach(() => jest.clearAllMocks());

  it('Does not log errors in console', () => {
    expect(errorSpy).not.toHaveBeenCalled();
  });

  it('Does not log warnings in console', () => {
    expect(warningSpy).not.toHaveBeenCalled();
  });
});
